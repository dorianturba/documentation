FROM squidfunk/mkdocs-material
RUN pip install pipenv
RUN pipenv install
RUN pip3 install mkdocs-awesome-pages-plugin
RUN pip3 install mkdocs-git-revision-date-localized-plugin
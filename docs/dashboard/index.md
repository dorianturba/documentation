# Dashboard

This section describes how the R2Devops Dashboard works and how to configure it.

- [📊 Learn how scores work in the Dashboard](/dashboard/scores/)
- [⚙️ Learn how to configure your Dashboard analysis](/dashboard/settings/)


## Overview

The first page of the dashboard is the overview page, offering a quick summary of projects within the current organization.

On this page, you'll find two global scores:

![Global scores](../images/global_scores.png)

### 🛡️ Security Score

The security score is calculated based on the following criteria:

- Visibility and protection of CI/CD variables used in the configuration file (_see Security/Variables tab_)
- Detection of secrets leaks inside the CI/CD configuration file; we also scan the merged configuration (_see Security/Secrets tab_)
- Status of container images used in jobs of the CI/CD configuration file (_see Security/Containers tab_)

### 🏆 Maintainability Score

The maintainability score is calculated based on the following criteria:

- Number of projects using reusable resources, such as templates and R2Devops templates
- Number of projects using up-to-date CI/CD resources (_see Job Usage tab_)

#### Inside the Projects Table

The maintainability status displayed on the projects table is based on the composition of the CI/CD configuration file, calculated according to:

- Compliance with templates and template usage
- Presence of hardcoded jobs and the number of lines

### 🤿 More In-Depth

#### Weight of Each Criterion

Each criterion has a weight used to calculate the global score, signifying that some criteria are more impactful than others.

#### Penalty System

To account for critical criteria, a penalty system is implemented. If a criterion is not met, the score is reduced by a certain percentage.

Current criteria with penalties:

- Secrets leaks inside the CI/CD configuration file
- Container image status declared as unsafe, indicating it comes from a non-official registry inside Docker Hub


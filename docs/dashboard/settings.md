# ⚙️ Project Analysis on R2Devops Platform

## Overview

Configure which projects within your organization to analyze. This guide provides a step-by-step walkthrough for using the R2Devops Dashboard to tailor the analysis settings to your specific projects.

!!! question "How it Works"
    Configure the projects you want to analyze directly from the R2Devops Dashboard. Once configured, the platform will automatically trigger a new analysis for the designated projects.

![Projects Analysis Configuration](../../images/projects_selector_access.gif)

## 📋 Requirements

### R2Devops Instance

An R2Devops self-managed instance is essential for utilizing the configuration of projects analyzed. If you haven't set up an instance yet, make sure to do so before proceeding.

### User Rights Necessary

#### - When your organization is a group in a GitLab instance

If your organization is **a group within a GitLab instance**, configuring projects to analyze requires at least **maintainer access** within this group.

#### - When your organization is the entire GitLab instance

If your organization is **the entire GitLab instance**, configuring projects to analyze requires **administrative privileges** for the GitLab instance.

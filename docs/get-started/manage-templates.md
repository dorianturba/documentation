---
title:  Manage template catalog
description: Learn how to manage a template catalog in R2Devops
---

# Manage template catalog

!!! info "What is a catalog?"
    A Catalog in R2Devops is a registry of GitLab CI/CD templates sourced from a git repository.

## 🔎 Guides

### 💾 Create a catalog

!!! info
    - Sources of R2Devops catalogs are GitLab repositories
    - You can store several templates per repository
    - You must have at least `Maintainer` access

1. Create a GitLab repository
1. In this repository:
    1. Create [R2 file](#template-r2-file)
    1. Create [CI/CD configuration](#template-cicd-conf)
    1. *(optional)* Create [documentation file](#template-documentation)
    1. *(optional)* Setup [versioning](#template-versioning)
    1. *(optional)* Create [changelog file](#template-changelog)
1. Login on R2Devops
1. In "Import templates" page: import your repository

### 🔄 Update a catalog

1. Update your template(s) in the GitLab repository
1. Create new version tag
1. Login on R2Devops
1. In "Import templates" page: click on refresh button

### ❌ Delete a catalog

1. Login on R2Devops
1. In "Import templates" page: click on delete button

## ⚙️ Template R2 file

Each template on R2Devops must have its own R2 file. It defines template
metadata.

- It can be located anywhere in a repository
- It uses the YAML format
- File extension have to be `.r2.yml`
- The file name is the template name

!!! example "Example of R2 file `docker_build.r2.yml` for the template `docker_build`"
    ```yaml
    files:
        template: ./docker_build/docker_build.yml
        documentation: ./docker_build/README.md
        changelog: ./docker_build/CHANGELOG.md
    data:
        description: "Build and publish a Docker image"
        icon: 🐳
        labels:
        - Docker
        - Build
        license: MIT
        deprecated: false
    ```

| Keyword | Description | Default |
| ------- | ----------- | ------- |
| `files:template` | (Mandatory) Relative path(*) to the template CI/CD configuration | ` ` |
| `files:documentation` | Relative path(*) to the documentation file | ` ` |
| `files:changelog` | Relative path(*) to the changelog file | ` ` |
| `data:description` | The template description | ` ` |
| `data:icon` | Icon of the template (see [emoji list](https://unicode.org/emoji/charts/full-emoji-list.html)) | `📋` |
| `data:labels` |  List of template labels (see list of available labels below) | `[]` |
| `data:license` | The template license | ` ` |
| `data:deprecated` |  Set the template as deprecated | `false` |

!!! note "Template visibility"
    The visibility of a template is the same than the visibility of its
    repository

!!! note "(*) Relative path"
    Path of files is relative from the location of the R2 file

??? question "Available labels"
    - API
    - AWS
    - Build
    - Code Performance
    - Code Quality
    - Composer
    - Configuration
    - Container
    - DAST
    - Database
    - Dependency management
    - Deploy
    - Docker
    - Documentation
    - GitLab
    - Golang
    - Gradle
    - Helm
    - Infrastructure
    - Java
    - Kubernetes
    - Lacework
    - Laravel
    - Lint
    - Maven
    - .NET
    - Npm
    - PHP
    - Python
    - Release
    - Robustness
    - S3
    - SAST
    - Security
    - SonarQube
    - Spelling
    - Terraform
    - Test
    - to be continuous
    - Twig
    - Utilities
    - Yarn

## 🛠️ Template CI/CD conf

This file, usually named with the template name with `.yml` extension contains
GitLab CI/CD configuration in `yaml` format. It can contains any piece of
configuration, from one keyword to a full pipeline. More info:

-  [GitLab CI/CD pipeline configuration
   reference](https://docs.gitlab.com/ee/ci/yaml/){:target=blank}
-  [R2Devops guidelines and best
   practices](/public-catalog/contribute/#guidelines) about job definition.

!!! example "Example of CI/CD conf"
    ```yaml
    docker_build:
      stage: build
      image:
        name: gcr.io/kaniko-project/executor:${IMAGE_TAG}
        entrypoint: [""]
      variables:
        COMMIT_CREATE_LATEST: "false"
        DOCKER_CONTEXT_PATH: ""
        DOCKERFILE_PATH: "Dockerfile"
        IMAGE_TAG: "v1.8.1-debug"
      script:
        - echo "Truncated example of script"
    ```

## 📚 Template documentation

This file, usually named `README.md`, contains the documentation of a template
in `markdown` format. It should explains what the template does, how to use it
and to customize it. A clear documentation is important: no one wants to use a
template without it.

!!! example "Example of documentation"
    ```md
    ## Objective

    Build a container image of your application from a Dockerfile at the root of
    your project, and push it to a remote registry or multiple ones. This is done
    using [kaniko](https://github.com/GoogleContainerTools/kaniko).

    ## How to use it

    1. Create a
    [Dockerfile](https://docs.docker.com/get-started/part2/#sample-dockerfile)
    (by default at the root of your project) to containerize your application
    2. Include template in your CI/CD conf (see quick use above)
    3. Customize the template if it's relevant

    ## Variables

    | Name | Description | Default |
    | ---- | ----------- | ------- |
    | `COMMIT_CREATE_LATEST` | In a commit context, also update `latest` tag | `false` |
    | `DOCKER_CONTEXT_PATH` | Path of build context from to repository root | ` ` |
    | `DOCKERFILE_PATH` | Path to Dockerfile from the build context | `Dockerfile` |
    ```

## 📄 Template versioning

Template versioning rely on source repository git tags. 2 options are supported
in R2Devops:

1. Tags scoped to all templates of the repository, without prefix. Ex: `1.2.3`
1. Tags scoped to only one template, prefixed with its path. Ex: `docker_build@1.2.3`

!!! note
    - The version `latest` is created using the `HEAD` of default branch
    - Versions should follows the [semantic versioning](https://semver.org/){:target="_blank"} (`MAJOR.MINOR.PATCH`)
    - The first version recommended for a template is `0.1.0`
    - Several templates can be versioned in the same repository

In order to **create automatically versioning** tags for your templates, you
have to create a [changelog file](#template-changelog) for each templates and
add the template
[template_release](https://r2devops.io/_/r2devops-bot/template_release) in your
repository CI/CD configuration.

## 🏗 Template changelog

This file, usually named `CHANGELOG.md`, contain the changelog of a template
following the [keep a changelog](https://keepachangelog.com/en/1.0.0/)
structure and using `markdown` syntax.

!!! example "Example of changelog"
    ```markdown
    # Changelog

    ## [0.4.0] - 2024-04-19
    * Add new option `--use-new-run` to kaniko executor (enabled by default)
    * Update kaniko image to `v1.5.1`

    ## [0.3.0] - 2023-11-25
    * New variable `DOCKER_USE_CACHE` to be able to cache layers of build
    * New variable `DOCKER_CACHE_TTL` to define time to live of cache
    * New variable `DOCKER_VERBOSITY` to set the verbosity of the build
    * New variable `DOCKER_OPTIONS` to be able to add additional options

    ## [0.2.0] - 2023-11-02
    * Add variable `DOCKERFILE_PATH` which permits specifying custom path to
    Dockerfile

    ## [0.1.0] - 2023-10-21
    * Initial version
    ```

---
title: Get started
description: Protect your Software Supply Chain with R2Devops! Stay informed with real-time CI/CD tracking, detect CVEs and ensure pipelines compliance
---

# Welcome

R2Devops is the platform to make CI/CD accessible for everyone in all projects:

- 🌏 Use and contribute to open source templates
- 🌏 Enjoy the GUI editor to create pipelines
- 🥇 Create a private template catalog for your organization
- 🥇 Check your CI/CD pipelines health for your entire organization

## ⚡ Get started

<div class="tx-card-container gap-2">
    <a alt="Open a ticket" href="./get-started/use-templates/">
    <button class="md-button border-radius-10 md-button" >
        📄 Use templates
    </button>
    </a>
    <a alt="Open a ticket" href="./get-started/manage-templates/">
    <button class="md-button border-radius-10 md-button" >
        💼 Manage templates
    </button>
    </a>
    <a alt="Open a ticket" href="./public-catalog/contribute/">
    <button class="md-button border-radius-10 md-button" >
        👨‍💻 Contribute to R2Devops CI/CD catalog
    </button>
    </a>
</div>

## 💬 Community

!!! heart "Community"
    We love talking with our contributors and users! Join our
    [Discord community :fontawesome-brands-discord:](https://discord.r2devops.io/?utm_medium=website&utm_source=r2devopsdocumentation?utm_campaign=addajob)

## 🙋 Support

- <a alt="Join R2Devops.io Discord" href="https://discord.r2devops.io?utm_medium=website&utm_source=r2devopsdocumentation&utm_campaign=homepage" target="_blank">Join us on Discord</a>
- <a alt="Open a ticket" href="https://tally.so/r/w5Edvw" target="_blank">Open a ticket</a>

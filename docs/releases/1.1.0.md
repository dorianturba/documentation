---
title: 1.1.0 release
description: > 
    It’s already been a month since the release of R2Devops V1.0.0. And our teams haven’t stop working! Let’s take a look at the new improvements on the platform. 👇
date: 2022-05-02
---
<p hidden>#more</p>

# **R2Devops 1.1.0 release**

**In April, we've listened to you and have implemented some features according to your feedback. Plus we keep on working on the product to make your life easier!**

---

## Major improvements

Some major improvements have been made on the design of the platform, with new features to display and sort the jobs.

### Row display for the job’s card
Job cards can now be displayed as rows or cards, it’s up to you!

![Display your job in column or cards in R2Devops](../../images/jobcards.gif)

### Filters column on the job explorer

We added a filter column on the job explorer page. You can now easily filter the jobs by labels and/or tier, and find the job you need without knowing it’s name!

![Filter the jobs easily in R2Devops](../../images/filtercolumn.gif)

### Sort jobs by Most recent or Best rated

Finally, we added a last option for you to sort the jobs on the Jobs’ explorer. You can choose to display them by order of last added, or regarding the grade they have!

![Sort your job in R2Devops](../../images/sort.png)

---

## Minor improvements

Some minor modifications were made, allowing a better navigation on the platform.

### Modifications of the theme toggle 

In can put R2Devops in light or dark mode. If you have created an account, you’ll see that the theme toggle is a lightsaber (directly issue of Star Wars movies)! 🤖

### Legal content added

Our team added the content of the pages `Terms of use` and `Legal Notice`.

### A new entry in the Personal Profile page

We updated an entry in the Personal `Profile page` of the Settings space. It’s the goodies code entry. We made a special launch and invited some people to join R2Devops with a goodies code.

### Job’s description have been truncate

In order to gain a bit of space on the cards, we had to truncate the job’s description on the cards of the `homepage` and the `Jobs explorer` page. We have also reduced the size of the cards in the `Jobs explorer page`.

### Tracking with no cookies

In order to see which actions are made on the website, we implemented some tracking events using Plausible. Plausible doesn’t use cookies. 
You can take a look at the [tracking dashboard](https://plausible.io/r2devops.io) for R2Devops!

### We are on Open Collective

You can now access from the footer our [Open Collective page](https://opencollective.com/r2devops), and support us in the development of R2Devops!

### Add a star on your favorite job’s repository

We added a section in the job description to give you fast access to the repository of the jobs you are using. You can see the numbers of stars and fork, and add a star in one click!

![Add a star in R2Devops](../../images/star.png)

### Modification of the form in the roadmap page

The form on the Roadmap page will be shown automatically after 3 seconds. We hope it will help gather feedback!

--- 

## Bug fixes

Finally, a few bugs were fixed during the last month.

### Allows dot in the username

Everything is said, you can now add a `.` in your username.

### Fix bug on navigation

When you were navigating on the `Jobs’ explorer`, they were some problem on the navigation. When you edited a specific search, you wouldn’t go back to the list of all jobs by clicking once again on the `Jobs explorer` link in the navigation. Now it will reset your search.

---

## Deprecation

To simplify the default pipeline, we are going to group both stages `static_tests` and `dynamic_tests` into only one: `tests`, after the build stage.

### Will you be impacted ?

* If you are not using `Official` job from stages `static_tests` or `dynamic_tests`: NO
* If you are using `Official` job from stages `static_tests` or `dynamic_tests` with only fixed versions (not latest): NO
* If you are using at least one `Official` job from stages `static_tests` or `dynamic_tests` with `latest` version: YES

### What's the impact

The pipeline configuration will be invalid because stage `tests` is not declared in your stages list but used in job(s).

### How to fix it

Update your stages list by adding `tests` and removing `static_tests` and `dynamic_tests` stages.

### Join us on Discord

In order to be informed of all the modifications we made on our platform in real time, you can join our [Discord Community](https://discord.r2devops.io?utm_medium=r2devops&utm_source=blog&utm_campaign=release_april){target=_blank}!



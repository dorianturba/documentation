---
title: 1.1.1 release
description: > 
    Let's take a look to all the improvements made in R2Devops in May 2022!
date: 2022-06-01
---
<p hidden>#more</p>

# **R2Devops 1.1.1 release**

**Time flies by, and R2Devops evolves! Let’s take a look at the new improvements on the platform. 👇**

---
## Major improvements

We added some new features during the past month. We even created new project on our GitLab’s account

### Creation of the project dashboard

Next to your jobs’ dashboard and settings, you can now access your projects’ dashboard. In here, once you linked your accounts, we display all your GitLab and GitHub projects!

![Projects’ dashboard in R2Devops](../../images/projectDash.png)

!!! info

     In order for R2Devops to display all your projects, you need to go to your `Personal space - account setting`, link your account and enter a personal token for each platform. [The steps are explained in the FAQ](https://docs.r2devops.io/faq-profile-token/).

### Use Pipeline Generator for your CI/CD

**⚠️this feature is currently disabled on R2Devops⚠️**
  

From our ‘Projects’ page, you can use Pipeline Generator! This new feature will scan your project and generate, using the [official jobs in the hub](https://r2devops.io/_/hub) a functional and efficient pipeline for it! With Pipeline Generator, building your CI/CD asks you 1 click. ✌️The feature works for projects hosted in GitLab or GitHub.

!!! info

     The number of projects for which you can use Pipeline Generator depends on your [license level](https://r2devops.io/pricing).

Today, Pipeline Generator can detect several technologies: 

* yarn
* npm
* mkdocs
* phpdocumentor
* phpunit
* composer
* apidoc
* dusk_test
* trivy_dependency
* mdbook
* cargo

### Customize the pipeline created with Pipeline Generator

Once you’ve triggered Pipeline Generator for your project, you can customize the pipeline produced by R2Devops, to customize the jobs implemented.
 


![Result of Pipeline Generator, with the Customize button](../../images/Autopipeline_result.png)


### Implementation of the license and payment system

With the Pipeline Generator feature, we implemented the license and payment system. You can see all the details of the license’s levels and the feature unlocked in the [pricing page](https://r2devops.io/pricing).
Regarding the payment system, we are using [Stripe](https://stripe.com/)! 
### Creation of generate.r2devops.io

Generate is a new page that will replace our actual pipeline page, where R2 helped you to build your pipeline with his questions. [Generate](https://generate.r2devops.io) works like Pipeline Generator, but will only give you access to a light-version of your pipeline. In order to see the whole thing, you need to create an account in R2Devops!

![Pipeline generated using Generate from R2Devops](../../images/generate.png)

---

## Minor improvements 

Along with the development of the new features, our technical team made some minor improvements in R2Devops. 👇

### New design for the homepage

You might have noticed, the design of the [homepage](https://r2devops.io) was completely rebuilt! We try to present more in deep the new feature Pipeline Generator, and simplify the user journey.

### Error message for GitLab and GitHub projects 

We updated the error messages on your projects’ dashboard. Now you’ll see specific messages explaining why we couldn’t generate a pipeline with Pipeline Generator:

* No valid token for your development platform,
* Username doesn’t exist,
* No project found,
* No more request available on GitLab or GitHub.


### Update of the navigation

Additionally to the homepage design changes, we upgraded the main menu. The search bar is now available only on the `hub` page, and the `roadmap` page was put in the footer. You can now access the `pricing` page from the top menu!

### Security enhancement in Pipeline Generator and Generate

**⚠️this feature is currently disabled on R2Devops⚠️**
  

In order to make sure the pipeline generated with Pipeline Generator and Generate won’t break, we decided to use fixed versions of the jobs. You can read [this article](https://blog.r2devops.io/blog/Good_practices/fixed_version_jobs/)  to understand how using fixed version of jobs is safer for your project!


### Job’ updates

During the past month, we updated many official jobs of R2Devops. Among them:

* [Php documentor](https://r2devops.io/jobs/build/phpdocumentor/) 
* [Owasp_dependency_check](https://r2devops.io/jobs/tests/owasp_dependency_check/)
* [Go unit test](https://r2devops.io/jobs/tests/go_unit_test/)
* [API doc](https://r2devops.io/jobs/build/apidoc/)
* [GitLab Terraform apply](https://r2devops.io/jobs/deploy/gitlab-terraform_apply/)
* [GitLab Terraform plan](https://r2devops.io/jobs/provision/gitlab-terraform_plan/)
* [Jest](https://r2devops.io/jobs/tests/jest/)
* [Link Checker](https://r2devops.io/jobs/tests/links_checker/)
* [Yarn Build](https://r2devops.io/jobs/build/yarn_build/)
* [Yarn Test](https://r2devops.io/jobs/tests/yarn_test/) 
* [Yarn Install](https://r2devops.io/jobs/.pre/yarn_install/)
* [Yarn Lint](https://r2devops.io/jobs/tests/yarn_lint/) 
* [Docusaurus Build](https://r2devops.io/jobs/.pre/yarn_install/)
* [Gulp](https://r2devops.io/jobs/others/gulp/)
* [NewMan](https://r2devops.io/jobs/tests/newman/)
* [Ng lint](https://r2devops.io/jobs/tests/ng_lint/)
* [NMP Build](https://r2devops.io/jobs/build/npm_build/)
* [NMP Install](https://r2devops.io/jobs/.pre/npm_install/)
* [NMP Lint](https://r2devops.io/jobs/tests/npm_lint/)
* [NMP Scripts](https://r2devops.io/jobs/others/npm_scripts/)
* [NMP Test](https://r2devops.io/jobs/tests/npm_test/)
* [Serverless Deploy](https://r2devops.io/jobs/deploy/serverless_deploy/) 

---

That’s it for May! Don’t hesitate to join the community to see in live the changes on R2Devops. See you next month. 👋

---
 
title: 1.2.0 release
description: > 
    Let's take a look to all the improvements made in R2Devops in August 2022!
date: 2022-09-01
---

<p hidden>#more</p>

# **R2Devops 1.2.0 release**

**You thought we weren't going to do anything all summer? Well, you're wrong! We've been working on new features for you! Let’s discover what changed in R2Devops in August.**

---
## Major improvements

We did some major changes on the website, especially regarding the jobs and how they work.

### Jobs are now linked to their organization

The major change of this month is on the jobs structure. Jobs are now linked to their organization. 
That means that only a member of your organization can access your private jobs.



!!! note "What you mean by organization?"

    By organization, we mean the GitLab root group!

### User can access private job from any of their organization

As we said, users can access private jobs from all their organization. You can be part of multiple organization and retrieve all the different jobs.

### Allow retrieving jobs by filtering on their namespace

We added a select input to filters organization names on the hub page when the private visibility tab is displayed! So you can now filter your jobs by their namespace.

![gif article Aout.gif](../../images/gif article Aout.gif)

## Minor improvements

Other changes were made on [r2devops.io](http://r2devops.io/), to enhance the navigation on the platform and ensure the best usability of our features for users.

### Improve pipeline generator results with JavaScript projects

**⚠️this feature is currently disabled on R2Devops⚠️**  
  
We are working on the generator pipeline to make it even more efficient. This month, we have been working on improving the results of the pipeline on JavaScript projects! Come and generate a perfect CI/CD pipeline for your JavaScript project now on [R2Devops](https://r2devops.io)!

### Fix bug in job search filter combination

A small bug had crept into the job search filter combination, but it’s fixed now and everything works correctly!
You can search for jobs on the [hub](https://r2devops.io/_/hub) for your project and use the filter without problem.

### Remove the regex unsupported by Safari browser

We realized that our site was inaccessible by using Safari, and we did not understand why. 
After some research, we realized that we were using a regex not supported by Safari, the issue is now solved!

### Job’ updates

During the past month, we updated many official jobs of R2Devops. Among them:

- [Pages](https://r2devops.io/jobs/deploy/pages/)
- [deploy_s3_terraform](https://r2devops.io/jobs/deploy/deploy_s3_terraform/)
- [eslint](https://r2devops.io/jobs/tests/eslint/)
- [netlify_deploy](https://r2devops.io/jobs/deploy/netlify_deploy/)
- [prettier_check](https://r2devops.io/jobs/tests/prettier_check/)
- [docker_build](https://r2devops.io/jobs/build/docker_build/)
- [venom](https://r2devops.io/jobs/static_tests/venom/)
- [pnpm_scripts](https://r2devops.io/jobs/others/pnpm_scripts/)
- [jest](https://r2devops.io/jobs/tests/jest/)
- [mega_linter](https://r2devops.io/jobs/tests/mega_linter/)
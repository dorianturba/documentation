# Project information
site_name: "R2Devops documentation"
site_description: "r2devops.io user documentation"
site_author: "R2Devops <cto@go2scale.com>"
site_url: "https://docs.r2devops.io/"

# Copyright
copyright: "Copyright &copy; 2023 R2Devops"
terms_of_use: "https://r2devops.io/termsofuse/"
privacy: "https://r2devops.io/privacy/"

# Repository
repo_name: "r2devops/documentation"
repo_url: "https://gitlab.com/r2devops/documentation"
edit_uri: "edit/main/docs/"

# Navigation
nav:
  - Get started:
      - Welcome: index.md
      - "Use templates": get-started/use-templates.md
      - "Manage templates": get-started/manage-templates.md
      - FAQ: get-started/faq.md
  - Public Catalog:
      - Concept: public-catalog/hub-concept.md
      - Contribute: public-catalog/contribute.md
  - Dashboard:
      - Home: dashboard/index.md
      - Scores: dashboard/scores.md
      - Settings: dashboard/settings.md
  - Self-Managed:
      - Home: self-managed/index.md
      - Architecture: self-managed/architecture.md
      - "⏱️ 5 min - Quick install": self-managed/local-docker-compose.md
      - Docker-compose: self-managed/docker-compose.md
      - Docker-compose & custom certificates: self-managed/docker-compose-custom-certs.md
      - Kubernetes: self-managed/kubernetes.md
      - Troubleshooting: self-managed/troubleshooting.md
  - Releases:
      - "1.34": releases/1.34.md
      - "1.33.3": releases/1.33.3.md
      - "1.31.1": releases/1.31.1.md
      - "1.28.3": releases/1.28.3.md
      - "1.27.0": releases/1.27.0.md
      - "1.22.0": releases/1.22.0.md
      - "1.21.0": releases/1.21.0.md
      - "1.20.1": releases/1.20.1.md
      - "1.18.0": releases/1.18.0.md
      - "1.17.0": releases/1.17.0.md
      - "1.16.0": releases/1.16.0.md
      - "1.15.0": releases/1.15.0.md
      - "1.14.2": releases/1.14.2.md
      - "1.3.1": releases/1.3.1.md
      - "1.3.0": releases/1.3.0.md
      - "1.2.3": releases/1.2.3.md
      - "1.2.2": releases/1.2.2.md
      - "1.2.1": releases/1.2.1.md
      - "1.2.0": releases/1.2.0.md
      - "1.1.3": releases/1.1.3.md
      - "1.1.2": releases/1.1.2.md
      - "1.1.1": releases/1.1.1.md
      - "1.1.0": releases/1.1.0.md
      - "1.0.0": releases/1.0.0.md
  - Ambassador: ambassador/index.md
  - Back to r2devops.io: https://r2devops.io

# Customization
extra:
  social:
    - icon: "fontawesome/brands/discord"
      link: "https://discord.r2devops.io/?utm_medium=website&utm_source=r2devopsdocumentation&utm_campaign=footer"
    - icon: "fontawesome/solid/globe"
      link: "https://r2devops.io/"
    - icon: fontawesome/brands/twitter
      link: "https://twitter.com/r2devops_io"
    - icon: "fontawesome/brands/gitlab"
      link: "https://gitlab.com/r2devops/documentation"
    - icon: "fontawesome/brands/youtube"
      link: "https://www.youtube.com/channel/UCVt39An0xR1PJaX3_Me_zkw"

extra_css:
  - css/extra.css
  - css/custom_admonitions.css

extra_javascript:
  - js/extra.js

# Configuration
theme:
  name: material
  custom_dir: overrides
  # Light and Dark Theme Toggle
  palette:
    - scheme: slate

  # Text
  language: "en"
  direction: "ltr"
  font:
    text: "Ubuntu"
    code: "Ubuntu mono"
  # Images
  favicon: "images/logo-documentation.svg"
  logo: "images/R2-documentation.png"
  features:
    - navigation.tabs
    - navigation.instant

markdown_extensions:
  - abbr
  - attr_list
  - admonition # block-styled content (note, idea, warning, ...)
  - codehilite # highlite code syntax
  - footnotes # references in footnotes
  - toc:
      permalink: true # anchor on titles
  - pymdownx.arithmatex
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.snippets
  - pymdownx.superfences
  - pymdownx.tabbed
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - pymdownx.highlight
  - meta
  - def_list
  - pymdownx.details

# Plugins (https://squidfunk.github.io/mkdocs-material/plugins/minify-html/)
plugins:
  - search # search input
  - minify:
      minify_html: true # minification of html
